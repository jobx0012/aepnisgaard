# Mini3: Job Ehrich
<br>
Velkommen til min **X3**
<br>
[link til program](https://jobx0012.gitlab.io/aepnisgaard/MiniX3/index.html)
<br>
Jeg har **igen** haft problemer med få lavet et link til koden. Koden virker perfekt lokalt, og kan godt uploades til gitlab, men når jeg prøver at oprette en pipeline står der fejl ved både "build" og ved "test" så der kommer desværre intet link. Nedenfor er der en demonstrationsvideo af hvordan programmet virker, og ellers er man desværre nød til at kopiere koden over i sin visual studio code hvis man skal prøve at køre programmet. <br> (husk at downloade billede og lydfiler også)
![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/MiniX3/x3.mov)
<br>
I forbindelse med opgaven skal der besvares nogle spørgsmål. Disse  vil jeg besvare herunder:
 <br>
<br>
 **Hvad ser du? Hvad handler programmet om?**
Når man kører programmet kommer der et hvidt canvas og en alternativ throbber begynder at rotere omkring centrum. Throbberen har rødder til den klassike throbber med pinde, som kan ses på denne gif <br> 
![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Ajax_loader_metal_512.gif/230px-Ajax_loader_metal_512.gif)
<br>
Men i stedet for at gentage samme animation om og om igen, er throbberen accumulerende og farver kanvas mere og mere. Ud over dette er den også interaktiv, så musens X-position controller afstanden mellem cirklerne, som udgør linjerne i throbberen, derfor kan man altså lave mønstre ved at bevæge musen.

Når throbberen har kørt indtil det meste af canvaset er farvet, vil de fleste filmkendere se et velkendt mønster på skræmen, nemlig riffelgangen fra jamesbondfilmene. Hvis man klikker med musen starter Monty Normans: James Bond theme.
I takt med at throbberen kører rundt, vil en jamesbondskikkelse også dukke frem i midten af Canvas. Til slutter kommer der blod ned af skærmen i takt med at throberen stopper. Dette er også kendt fra jamesbondfilmene.

**Hvordan er det lavet? Hvilke (nye/interessante) syntakser bliver brugt?**
Der er gjort brug af p5.js sound library til musikken.
Til throbberen er der gjort brug af push/pop til at begrænse nogle indstiller til dele af koden.
Rotate er brugt til at rotere koden rundt i cirkler
 Forloop er brugt til at tegne en masse cirkler, som til sammen danner en linje.
MouseX er brugt til at variere cirklerne der udgør linjerne.
Der er også gjort brug af billeder, og resize til at ændre på billedernes størrelse.
Der er også gjort brug af translate, til at forskyde 0,0 til midten af canvas i stedet for oppe i venstre hjørne.

<br>
<br>
Jeg stødte på flere problemer i forbindelse med at lave koden. Det største problem var i forbindelse med det blod som løb ned af skærmen, for throbberen blev ved at gå ind over blodet, så jeg var nødt til at få throbberen til at stoppe i forbindelse med at blodet kom ned af skærmen.
<br>
Havde også problemer med lyden, da jeg ikke kunne få den til at starte uden at man selv skulle aktivere den med et musseklik.
 <br><br>
**Hvad får det dig til at tænke på eller føle?**
<br>
Throbbere er som oftest forbundet med frustration og ventetid, hvilket vi som mennesker i 2023 har mindre og mindre tålmodighed. For bare 10 år siden kunne det tage flere minutter at tænde en computer og nu skal der gerne gå mindre end 10 sekunder for at brugeren er tilfreds. Firmaer som google har gennem tiden forsøgt at forsøde ventetiden ved f.eks. At tillade brugeren at spille snake med throbberen <br>
![](https://thumbs.gfycat.com/DaringEducatedAmericancrow-size_restricted.gif) 
 <br>
	
eller dino-run: <br>
![](https://media0.giphy.com/media/KeQgaiv19rCEdVFnW8/200w.gif?cid=6c09b952ibpf6d5nkfadugvhc7mzbwb7nwhcrtivppt2z39n&rid=200w.gif&ct=g)
<br>
Throbberen har også et aspekt af dette, hvor den er interaktiv og dermed er "ventetiden" som den også indikerer ikke ligeså kedelig. Her udnytter man altså throbberen til at lave kunst; sin helt egen loadingskærm. Det kunne f.eks. Være til et james Bond computerspil, hvor loadingskærmene blev udskiftet med denne interaktive version..

<br>
<br>
Chao

*- Job Ehrich* <br>
<br>

