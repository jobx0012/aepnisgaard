# MiniX1: Job Ehrich
Velkommen til min **X1**.
![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/MiniX1/Sk%C3%A6rmbillede_2023-02-14_kl._18.49.28.png)
<br>
Hvis du har lyst, kan du starte med at [klikke her](https://jobx0012.gitlab.io/aepnisgaard/MiniX1/index.html) for at åbne den RunMe, som det hele kommer til at handle om. <br>

I forbindelse med opgaven skal der besvares nogle spørgsmål. Disse  vil jeg besvare herunder:
 <br>
<br>
 **Hvad har jeg skabt?** <br>
 For at gøre en lang historie kort, var jeg d. 9 januar til et inspirerende kaffekursus på *La Cabra*, og det endte med at der opstod et pludseligt behov i mit liv for lækker kaffe. Gennem intensiv og grundig research har jeg fundet frem til den mest økonomsiske måde at lave **god** kaffe på, og wow, det var alt arbejdet værd! Alle der har smagt den har været helt forbløffet over at man kan lave så god kaffe på under 4 minutter, og **uden** en **dyr** espresso maskine.

 Men nu vil de alle sammen have opskriften (som *jeg* i øvrigt ikke har lavet. Jeg har tyvstjålet den [her fra](https://www.youtube.com/shorts/b_jiA2jZjXw)). og derfor har jeg nu lavet [mit X1 projekt](https://jobx0012.gitlab.io/aepnisgaard/test/index.html), som er en form for animeret guide til *aeropress* - men den er ikke animeret - den er `//kodet.`
 <br>

**Hvordan vil jeg beskrive min første selvstændige kodning oplevelse??** <br>
Det har kort fortalt været en rigtig god oplevelse. Først og fremmest skulle jeg få en idé og tænke mig frem til hvordan jeg helt konkret skulle få den manisfæsteret. Idet at den skulle kodet satte det helt sikkert nogle strenge rammer for omgaver, primært fordi jeg intet vidste, jo jeg flere komplicerede elementer jeg skulle inkludere, jo mere tid krævede opgaven. Samtidigt var jeg også nødt til at ændre nogle af mine ambitioner undervejs, så man kan sige at mediet i høj grad har været med til at forme slutproduktet.
Jeg benyttede mig af kommandoer som `textStyle()`,`frameCount`, `loadImage()`,`function mouseCliked()`, `let =` og den klassiske `if-else`.Alle var nogen som jeg skulle læse mig frem til på [referencelisten.](https://p5js.org/reference/)
<br>Når jeg undervejs stødte på problemer, kunne jeg gå til referencerne, og i værste fald kopiere noget af koden over, og modificere den.
<br> <br>
 **Hvordan er kodningsprocessen iforhold til at læse og skrive tekst?** <br>
 Jeg synes processerne er meget forskellige. Lige nu er det også fordi at jeg har så begrænset erfaring med at kode. Jeg koder jo imens jeg lærer at kode. Læse og skrive det kan jeg jo allerede iforvejen. Det føles også som om man arbejder i to lag samtidigt når man koder. Du har både din *code editor* og din *live server*, hvorimod når man læser og skriver arbejder man kun i et lag. En pdf f.eks. eller i Word. Det betyder også at når man har ambitioner om at skabe noget i outputtet, og man laver sit input, kan det endte med at se anderledes ud end man havde forestillet sig.
<br>
<br>
**Hvad betyder kodning og programmering for mig, og hvordan har teksterne til uge 6 hjulpet mig med at reflektere over disse termer?** <br>
Før semesterstart har kodning og programmering ingen betydning haft for mig. Jeg havde en forestilling om at jeg glædede mig til at lære det, men jeg havde ingen erfaring og har aldrig tænkt særligt meget over kodning og software i mit liv. Jeg vidste at man brugte det til at lave hjemmesider og software, men der sluttede min viden også. Teksterne i uge 6 har fået mig til at reflektere over at kodning ikke kun er en teknisk færdighed. Det tror jeg ellers at de fleste mennesker går rundt og tænker. Og kodning er heller ikke neutralt. På samme måde som vi har lært om design i *Interaktions- og Interfacedesign*  kan kodning også have både sociale, kulturelle og politiske betydninger og konsekvenser. Helt konkret hvordan det ser ud i praktisk glæder jeg mig til at finde ud af, for da jeg kodede mit [X1](https://jobx0012.gitlab.io/aepnisgaard/test/index.html) til i dag, følte jeg primært at kodningsprocessen var teknisk. 
<br>
<br>
**Hvad er forskellen i at læse andres kode og at skrive din egen kode? Hvad kan du lære ved at læse andres miniX?**
<br> Det er to meget forskellige processer at læse andres kode, og skrive kode selv. Når man skriver kode selv starter man fra et blankt canvas, og skal selv finde på og skabe noget ud fra dette udgangspunkt. Når man læser andres kode og ser deres RunMe, starter man med slutproduktet, og kan ud fra dette få noget andet ud af det, ved f.eks. at se nærmere på koden og prøve at forstår meningen med projektet. Herved kommer man om bag projektet og får en større forståelse mellem kode og det færdige program. Og når man skal give feedback lærer man også at tale om det og tage stilling til det.
<br>
<br>
Chao Chao

*- Job Ehrich* <br>
<br>

────────────────────────────────────────
────────────────────────────────────────
───────────████──███────────────────────
──────────█████─████────────────────────
────────███───███───████──███───────────
────────███───███───██████████──────────
────────███─────███───████──██──────────
─────────████───████───███──██──────────
──────────███─────██────██──██──────────
──────██████████────██──██──██──────────
─────████████████───██──██──██──────────
────███────────███──██──██──██──────────
────███─████───███──██──██──██──────────
───████─█████───██──██──██──██──────────
───██████───██──────██──────██──────────
─████████───██──────██─────███──────────
─██────██───██─────────────███──────────
─██─────███─██─────────────███──────────
─████───██████─────────────███──────────
───██───█████──────────────███──────────
────███──███───────────────███──────────
────███────────────────────███──────────
────███───────────────────███───────────
─────████────────────────███────────────
──────███────────────────███────────────
────────███─────────────███─────────────
────────████────────────██──────────────
──────────███───────────██──────────────
──────────████████████████──────────────
──────────████████████████──────────────
────────────────────────────────────────
────────────────────────────────────────

