//etablerer en masse globale variabler
//de fleste af dem er koblet til forskellige noisefunktioner, resten af dem er koblet på randomfunktioner

let xoff = 0.1
let xxoff = 1000000
let yoff = 100
let zoff = 5000
let yyoff = 400
let rand1
let rand2
let rand3
let rand4
let rand5
let randOffset = Math.random() * 200
let randOffset1 = Math.random() * 50
let randOffset2 = Math.random() * 50
let randOffset3 = Math.random() * 50
let randOffset4 = Math.random() * 50
let z = Math.random() * 140 
let x = 440+z
let y =100+z
let delta = Math.random() * 14+1
let alfa = Math.random()*30+2
let bravo = Math.random()*2


//canvas, ramme, variabel frameRate
function setup() {
    createCanvas(windowWidth, windowHeight)
    angleMode(DEGREES)
    background(0)
    fill(255)
    rect(width / 4, 0, width / 4 * 2, height)
    fill(180, 100)
    strokeWeight(2)
    rect(width / 5 * 1.5, height / 9, width / 5 * 2, height / 1.17)
    frameRate(alfa)
}

function draw() {
    //noisefunktioner
    let rand1 = noise(xoff) * 100
    let rand2 = noise(yoff) * 100
    let rand3 = noise(yyoff) * 100
    //den grønne/orange farve, men noise
    fill(rand1 * 4 - 90, 200 - rand3, rand2 * 2 - 140)

    //opsætning af tekst osv
    push()
    stroke(0)
    fill(0)
    textAlign(CENTER)
    textSize(50)
    text('PANTONE PALETTE 1992', width / 2, 60)
    pop()
    push()
    strokeWeight(0.5)
    stroke(0)
    noFill()
    textAlign(CENTER)
    textSize(10)
    text('Most popular palette of 1992, "trees and rocks"', width / 2 + 180, height - 10)
    pop()

    //forskellige stroke

    if (floor(bravo)==0){
        noStroke() 
    }
    if (floor(bravo)==1){
        strokeWeight(1)
    }


    // WHILE FUNKTION SOM LAVER CIRKLERNE
    while(x < 1010-z){ // x position på cirklen. 1010 er bredden på rammen. Z er random størrelse
        ellipse(x, y, z+10 ,z+10); //Laver en cirkel på X,Y som i starten er defineret som hjørnet i rammen.
        x = x+z+delta;//delta gør afstanden mellem cirklerne lidt længere. Z er størrelsen, så de får den rigtige afstand
        }
       if(x => 1010-z){ //når x rammer bredden, så sætter vi X tilbage til venstre kant og rykker en linje ned. Så går whileloopet igang i gen.
        x = 440+z
          y = y + z+delta
          console.log(10)
       }
       if(y>700-z){ //når y rammer bunden af rammen pauser programmet i to sekunder og loader websiden igen efter
        setTimeout(reload,2000)
       noLoop() 
        //y=100+z 
       // fill(180)
        //strokeWeight(2)
        //rect(width / 5 * 1.5, height / 9, width / 5 * 2, height / 1.17)
       }
        

    xoff += 0.2
    yoff += 0.01
    zoff += 0.008
    xxoff += 0.005
    yyoff += 0.1

}

function mousePressed() {
    saveCanvas('Strynø designmesse', "png");
  }

  function reload(){
    location.reload(true)
  }