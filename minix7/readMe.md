# MiniX7: Job Ehrich
<br><br>
Velkommen til min miniX7
<br><br>
[link til program](https://jobx0012.gitlab.io/aepnisgaard/minix7/index.html)
<br>
<br>
[link til min gamle miniX](https://jobx0012.gitlab.io/aepnisgaard/miniX5/index.html)

![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/minix7/pantone.png)

<br>
<br>
### genbesøg miniX
<br><br>
Jeg har genbesøgt min miniX5, da jeg glemte at lave et for-loop og det var et krav.
Jeg tog af mine ideer til min miniX5 og videreudviklede den.
<br><br>
Den gamle så sådan her ud:
Her kunne jeg godt lide udtrykket og udseendet, men jeg synes desværre ikke den var generativ nok. Det var meget overskueligt hvad der ville ske og det var i bund og grund bare nogle forskellige vertex som bevægede sig organisk. Så nu prøvede jeg at lave et nyt design som var mere generativt, men stadig noget jeg synes var visuelt flot. Jeg ønskede også at bruge et while-loop, da jeg ikke har brugt det før, så det ville jeg også undersøge.
<br><br>
Mit program er en fiktiv plakat til pantone's årets farve palet fra 1992, altså en serie populære farver fra dette år, og så illustrerer mit program på mange forskellige måder disse farve, fælles for det hele er at den gør det med cirkler. Hver gang programmet starter bliver der valgt nogle random værdier og disse bestemmer størrelse på cirklerne, afstanden, om der skal være stroke eller ikke og også hvor hurtig framerate skal være, altså hvor hurtigt programmet kører igennem. Når programmet er køret igennem, venter den 3 sekunder hvorefter den refresher hele siden, og programmet kører forfra med nogle helt andre random-værdier.
<br><br>
Den største udfordring var helt sikkert at få cirklerne til at blive inden for et begrænset område, da de hele tiden varierede i størrelse, og de enten gik for langt ud til siden, eller var forskudt til venstre i området eller andet. Det blev ikke perfekt men det blev okay. 
<br><br>
Så har jeg også brugt nogle nye javascript syntakser som ´SetTimeout(function, delay)´og ´location.reload(true)´
<br><br>
Jeg har også i forbindelse med min refleksion om generativ kunst tilfølet en anden ny syntaks, nemlig: ´saveCanvas(navn, filtype)´
<br><br>
Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?
<br><br>
Jeg har tænkt over i hvilken del af processen generativ kunst bliver til kunst. Koden i sig selv har jo ingen æstetisk kvalitet for det normale menneske, især ikke over andre koder. Så er det selve runMe'en der er kunsten. Og hvis dette er tilfældet, er det så dårlig /grim kunst når programmet genererer noget grimt, eller er det godt hvis den potentielt kan lave noget flot. Hvad nu hvis programmet kører uden nogen ser det, og danner den flotteste plakat nogensinde, men ingen ser det, og det måske aldrig sker mens nogen ser det? Jeg synes i hvert fald digital kunst er et samspil mellem, kunster værktøj og modtager. Derfor har jeg også givet modtaget mulighed for aktivt at tage stilling til kunsten og gemme de som DE synes er flotte. Ved at man klikker når man har lyst og derfor gemmer et billede af skærmen, ned på sin computer.
<br><br>
Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?

Denne MiniX er ikke lige så kritisk som nogle af de eksempler som vi ofte har kigget på i undervisningen, da det ikke eksponer nogle skjulte magtstrukturer, sexisme eller racisme. I stedet er den med til at undersøge og starte en samtale om generativ kode, kunst, rettigheder og ejerskab. Men faget ÆP har også til formål at få os til at reflektere over de here dybere lag, som der ofte/altid er i digitale medier som er kodet, og som ofte er meget usynlige:

"it is important to further explore the intersections of technical and conceptual aspects of
code in order to reflect deeply on the pervasiveness of computational culture and its social and
political effects — from the language of human-machine languages to abstraction of objects,
datafication and recent developments in automated machine intelligence, for example (Winnie Soon and Geoff Cox, intro)

Hvor ordet æstetik egenlig betyder en form for skønhed og det sanselige, så handler æstetisk programmering om "sense making & world building"(lodahl rolighed, powerpoint ÆP modul 1a)

Such practical “knowing” also points to the practice of “doing thinking,” embracing a plurality of ways of working with programming to explore the set of relations between writing, coding and thinking to imagine, create and propose “alternatives(Winnie Soon and Geoff Cox, intro)

Æstetisk programmering er altså mødet mellem hard skills, altså kodning "håndværket" og så softskills, som er reflektion og kritisk tænkning.


<br><br>
<br>
slut
