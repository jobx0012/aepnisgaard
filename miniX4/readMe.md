# MiniX4: Job Ehrich
<br>
Velkommen til min **X4**
<br>
[link til program](https://jobx0012.gitlab.io/aepnisgaard/miniX4/index.html)
<br>

![](https://gitlab.com/jobx0012/aepnisgaard/-/blob/main/miniX4/Sk%C3%A6rmoptagelse_2023-03-12_kl._20.51.19.mov)
<br>

**a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival**

# Y0u are in contr0l
<br>
Accept all cookies. Accept neccesary cookies. Accept our terms and conditions. GDPR. Public, Private. You are in control of your life, you have the rights over your own data. The service is here for you. Why waste your time on bad content. Targeted adds. Targeted content. You are in control. You are. Want to delete your account? Are you sure? Confirm. Confirm email. Confirm telephone. Wait.
<br>
This program wants you to be more aware of the things hidden behind the beautiful aestetic interface and bright clinical colours, and millions of manipulative checkboxes where i don't have 14 hours to read all 200 pages of terms and conditions. You think you are in control, because you get the illusion that you are, because they want you to feel that you are, but you have no idea.
<br>
<br>
<br>


**Describe your program and what you have used and learnt + how your program and thinking address the theme of “capture all."**
<br>
Dette værk har til sinde at tydeliggøre en problemstilling vi alle sammen oplever på daglig basis. Når vi færdes på internettet, om det er google, facebook, tiktok osv. bliver der som vi er bekendt med altid indsamlet en masse data. Vi ved godt at der samles date om os, men de færreste af os er klar over præcist hvilke data det drejer sig om, og selvom man læser alt om det, er det helt umuligt at finde ud af præcist hvordan de store techvirksomheder bruger denne data. Med andre ord får vi en tryghedsillusion om at vi controllerer vores data. Vi kan selv vælge om vi klikker på "alle cookies" eller "tillad kun nødvendige" og vi har indstillinger og gdpr, men bag et flot og simpelt interface gemmer der sig systemer og algorytmer der uendeligt fjernt fra os, og langt fra vores kontrol. Og det værste er at det er så begrænset hvad vi kan gøre ved det, for mange af os er dybt afhængige af bl.a. vores google og facebookkonti, til arbejde, foreninger, studie og andre aktiviterer, for at nævne nogen. Mit værk Y0u are in contr0l giver brugeren to oplevelser. Både oplevelsen af at have control over programmet. Baggrunden responderer på musens position, øjet følger musen og de farvede bølger følger lydniveauet. Men samtidigt får bruger også oplevelsen af, at man ikke har kontrol over noget som helst. Øjet, baggrunden og bølgerne har nemlig samtidigt sit helt eget liv, og man kan som bruger blive i tvivl om hvem der egenligt har kontrollen - dig eller programmet.
<br>
I programmet har jeg lagt meget energi i at forstå og benytte `noise()` funktionen. Jeg har på det seneste set en stor del digital kunst som gjorde brug at funktionen, og efter jeg læste en kunster skrive at det var hendes all-time-yndlingsfunktion, besluttede jeg mig for at forstå funktionen til fulde. Det bundende i mange timers youtube og træning, men nu kan jeg til dels finde ud af at bruge funktionen, og jeg er helt vild med den. Kort fortalt er noise() en random() funktionen, men i stedet for at være HELT tilfældig, vælger den altid en tilfældig værdi tæt på den sidste værdi. Det gør at når det f.eks. Bruges til animation og bevægelse ser bevægelserne meget mere naturlige ud - næsten som om at det lever sit eget liv. Det kan bl.a. ses på øjet som svæver uhyggeligt rundt, næsten som var det levende. Og også baggrunden som ser organisk ud på en måde. Og lyderbølgerne. Det er denne del af koden jeg har brugt til at give en form for artificial inteligence effekt, som om at programmet har sit eget liv, og bestemmer. En sjov detalje er at noise funtionen fungerer ved at aflæse nogle tal på en graf der bliver skabt ud fra et seed, som vælges tilfældigt hver gang programmet startes, og man kan vælge hvor fra på denne graf at tallene skal aflæses. I programmet har jeg valgt af aflæse de samme tal til både øjet og lydmuren, og hvis man lægger mærke til deres bevægelse, kan man se at de knyttet sammen. De bevæger sig samtidigt og accelerer samtidigt og lige hurtigt osv. Det er ret fascinerende, og det synes jeg generelt alt ved Noise() funktionen er.
<br> <br>
Ud over dette har jeg benyttet mouseX og mouseY for at tage brugerinput, og alt det nye vi har lært med at brugt computerens lydniveau. Jeg har desværre ikke kunne benytte kameraet og facetracker da mit webcam er gået i stykker. Jeg har også benyttet function keyPressed, med ENTER knappen (keyCode 13).
<br>
Sammenspillet mellem de ting man styrer som bruger og noise() som er alt det man ikke styrer, og som styrer sig selv, giver en effekt af at man har indflydelse men ingen kontrol, og det er hele budskabet og meningen med mit værk.
<br>

**What are the cultural implications of data capture?**
<br>
Jeg  har den oplevelse, at data capture har gjort et uhensigtsmæssigt lille indtryk på os mennesker. Genenrelt virker det til at de færreste forholder sig til hvad de egentligt siger "ja" til, og vi sælger os selv og vores data for at benytte gratis tjenester, som så ikke er gratis. Data er nutidens olie, guld og diamanter på én gang, og det virker til at være de færreste som stiller spørgsmålstegn ved hvor meget deres opmærksomhed og data er værd. Tænk hvis facebook tjener 500 kroner på DIG om måneden, og du går med en illusion om at det er en gratis ydelse. Vil man så ikke hellere betale et månedligt beløb og så FAKTISK have kontrol, (og færre reklamer)? Tænkt hvis det er mange tusinde kroner om året de store virksomheder tjener på at sælge DIN information til højstbydende? Det virker som om at de fleste ikke tænker over disse ting. Nok fordi at den teknologiske udvikling er sket så hurtigt, og det samtidigt alt sammen er skjult i koden, bagved et flot venligt design. Men det er heller ikke fordi folk ikke er opmærksomme på det. Alle ved at data er en stor ting, og der bliver tit snakket om det i medierne. Seneste om Tiktok. Problemet, tror jeg, ligger i at det er alt for ukonkret, og vi egenligt ikke ved præcis hvad der sker "deromme" samt at der INGEN gode alternativer er. Man kan spille med på deres regler eller isolere sig fra nettet, og for 99/100 er nr. to ikke en realistisk mulighed i 2023. Jeg læste i forbindelse med en fremlæggelse jeg havde om "google will eat itself" at kunsterne bagved, ønskede et form for ministerium i staten der alene arbejder med love og regler for online teknologi, og det synes jeg faktisk er en rigtig god ide. Det er også noget af det EU arbejder med, og det er alt sammen med til at kæmpe imod den kæmpe magt, virksomheder som google og meta har, fordi de næsten har monopol på deres markeder.

<br>
<br>
Chao

*- Job Ehrich* <br>
<br>

