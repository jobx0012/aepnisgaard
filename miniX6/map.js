class Tomato{
    constructor (x,y,w,h) {
        this.posX = x
        this.posY = y
        this.speed = random(1,50)
        this.h = h
        this.w = w
    }

    show() {
        fill (73, 179, 68)
        noStroke()
        rect(this.posX,this.posY,this.h,this.w)
    }

    
}