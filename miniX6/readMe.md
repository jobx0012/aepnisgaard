# MiniX5: Job Ehrich
<br><br>
Velkommen til min miniX5
<br><br>
[link til program](https://jobx0012.gitlab.io/aepnisgaard/miniX6/index.html)
<br>
<br>
![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/miniX6/Sk%C3%A6rmbillede_2023-03-26_kl._23.41.10.png)
<br>
<br>
### Dadel's Quest
<br><br>
Mit spil hedder Dadel's Quest og tager udgangspunkt i hovedpersonen Jonas Dadel, som skal navigere igennem et form for dungeon, hvor han ikke må ramme væggene. Spillet tager udganspunkt i flere forskellige spil fra min barndom. Både dungeoncrawler spil, hvor man navigerer fra rum til rum, som legend of zelda, men også det sjove "practical joke spil" scary maze, som mange kender til. Scary maze er et snyde spil, hvor man skal koncentrere sig om at bevæge musen igennem en bane uden at ramme kanten, og i niveau 3, hvor det begynder at blive svært og man skal koncentrere sig meget, dukker der pludselig et såkaldt "jump scare" op på skærmen, som skræmmer brugeren. Det havde vi det sjovt over.
<br>
<br>
Dadel's quest gør brug af to objekter. Det ene er "player" som er det svævende hoved. Dette kan bevæge sig rundt på banen vha. piletasterne. 
Det andet objekt er alle murerne i spillet. Når jeg ser tilbage på det, var det nok ikke det smarteste at bruge objekt til at bygge banen med, da min kode er blevet meget rodet, men den fungerer, så jeg har valgt ikke at lave det om. Men det er smart at bruge "objekt" til "player" da man kan få en helt fungerende person at spille med, med meget lidt kode.
<br>
<br>
Når spillet starter er man i "rum nr 0" og når spillerens y-værdi er mindre end 0 (altså når man går igennem toppen af canvasset) så er der et if-statement der aktiverer så man kommer i "rum nr 1" og det nye rum loader, osv. Dette gælder også hvis man går tilbage til tidligere rum. "murerne" som fungerer som forhindringer i spillet må ikke røres af spilleren. Hvis spilleren rører murerne har spilleren tabt og skærmen viser "game over" og afspiller en melodi der passer dertil. Da alle firkanterne der bliver brugt til at lave murerne har forskellige størrelser var det for svært for mig at bruge *dist* mellem spiller og mur til at aktivere "game over" så det er blot et simpelt if-statement med x og y-værdi som aktiverer dette. Når spilleren komer til rum nr 4 bliver det lidt sværere og når spilleren skal igennem den smalleste gang bliver der aktiveret et jumpscare. Her får spilleren måske et shock hvorefter det går op for dem, at spillet er et snydespille med det formål at give dem en lille forskrækkelse.
<br><br>
Jeg bruger nogle nye syntakser i denne kode. For det første objekter som er helt nyt, og lidt svært at forstå, men også meget smart og effetivt i nogle situationer. Jeg bruger også lydeffekter og soundtrack. Og en specifik skrifttype jeg har downloadet og indsat også.
<br><br>
Når man bruger objectabstraction i sit program vælger man nogle specifikke egenskaber som man vil have repræsnteret. Nogle specifikke udseendemæssige egenskaber og nogle bevægelsesmønstre eksemplevis. Andre ting udlader man også. Det er interessant at se på hvilke ting som specifikt bliver tilvalgt og fravalgt ved denne repræsentation: 
<br><br>
"Object abstraction in computing is about representation. Certain attributes and relations are abstracted from the real world, whilst simultaneously leaving details and contexts out. (Soon & Cox, 2020, p. 146)"
<br><br>
Denne måde hvorpå vi vælger specifikke attributer og fravælger andre, og dermed karakteriserer menneske ud fra disse, er interessant at se på, for det sker også mange andre stedet. F.eks. Når man skal oprette en facebookprofil, bliver profilen og dermed også det billede algoritmen tegner af mennesket bagved, karakteriseret ved nogle få udvalgte egenskaber. Det kan være seksualitet, geografi og alder. Det er selfølgelig umuligt at få alle facetter med af et menneske, men det er værd at overveje om det også kan have konsekvenser for et individ eller for oplevelsen at man bliver sat i en boks på den måde:
“It’s also worth reiterating that OOP is designed to reflect the way the world is organized and imagined, at least from the computer programmers’ perspective.” (Soon & Cox, 2020, p. 160)
<br><br>
Også når vi snakker om overvågning og algorytme på det plan, og de dystopiske ideer om en algorytme som vurderer menneske hele tiden på gaden og beregner den potentielle trussel for staten ud for menneskets adfærd, ting som der måske foregår i mindre grad i Kina allerede nu, så er det værd at reflektere over hvilke konsekvenser det kan have at vurdere X-antal parametre på et menneske og sætte dem i en bås på baggrund af disse, især når det ikke bare er på nettet men lige pludselig i det virkelige liv.
<br><br>
<br>
slut
