
let xoff = 5
let yoff = 2000


class Player {
    constructor(){
        this.posX = width/2
        this.posY = height*0.8;
        this.speed = 50
        this.speed1 = noise(yoff)*10
        this.size = 50
        xoff += 0.1
        yoff += 0.1
        
    }
    show(){
        rectMode(CENTER)
        push()
        imageMode(CENTER)
        //fill(87, 193, 217)
        //rect(this.posX,this.posY,this.size)
        image(img1,this.posX,this.posY,this.size,this.size)
        pop()
    }
    moveUp(){
     this.posY -= this.speed
    }

    moveDown(){
        this.posY += this.speed
    }
    moveLeft(){
        this.posX -= this.speed
        if (this.posX<-this.size) {
            this.posX = width+this.size
            }
    }
    moveRight(){
        this.posX += this.speed
        if (this.posX>width+this.size) {
            this.posX = 0-this.size
            }
    }

}