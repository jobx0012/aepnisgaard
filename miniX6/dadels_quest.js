let player;
let counter = 0
let myFont;
let tal;
function preload() {
    myTrack = loadSound('8bit.mp3');
}
function setup() {
    createCanvas(500, 500)
    background(240)
    player = new Player()
    tomato = new Tomato(50, height - 50, 400, 300)
    tomato1 = new Tomato(width / 2 - 100, height / 2, 100, 400)
    tomato2 = new Tomato(width, height - 50, 900, 50)
    tomato3 = new Tomato(0, height - 50, 900, 50)
    tomato4 = new Tomato(width / 2 + 100 + 100, 0, 200, 200)
    tomato15 = new Tomato(100, 0, 400, 200)
    tomato5 = new Tomato(width - 50, height - 50, 900, 300)
    tomato6 = new Tomato(width / 2, height - 10, 20, 100)
    tomato7 = new Tomato(50, height - 50, 900, 300)
    tomato8 = new Tomato(width / 2 + 200, height - 50, 100, 100)
    tomato9 = new Tomato(0)
    tomato10 = new Tomato(0)
    tomato11 = new Tomato(0)
    tomato12 = new Tomato(0, 0, 450, 1000)
    tomato13 = new Tomato(0, height - 50, 1000, 400)
    tomato14 = new Tomato(width / 2 + 200, height - 50, 350, 300)
    img = loadImage('giphy.jpeg');
    mySound = loadSound('LS.mp3')
    img1 = loadImage('m.png');
    myFont = loadFont('game_over.ttf')
    lydGO = loadSound('GO.mp3')
    lydP = loadSound('P.wav')
}

function draw() {
    if (frameCount > 180 && !myTrack.isPlaying()) {
        myTrack.play();
    }
    if (frameCount < 180) {
        fill(0)
        rect(0, 0, 1000, 1000)
        fill(255)
        textAlign(CENTER)
        textFont(myFont, 150);
        text("DADEL'S QUEST", width / 2, height / 2)
    }
    else if (frameCount < 360 && frameCount >= 180) {
        fill(0)
        rect(0, 0, 1000, 1000)
        fill(255)
        textAlign(CENTER)
        textFont(myFont, 150);
        text("Sound on", width / 2, height / 2)
    }
    else {
        background(240)
        //kollision()
        player.show()
        if (player.posY < 0 - player.size) {
            player.posY = height
            counter = counter + 1
        }
        if (player.posY > height + player.size) {
            player.posY = 0
            counter = counter -1
        }
      
        if (counter == 0) {
            tomato5.show()
            tomato6.show()
            tomato7.show()
        }
        if (counter == 1) {
            tomato5.show()
            tomato7.show()
        }
        if (counter == 2) {

            tomato.show()
            tomato1.show()
            tomato2.show()
            tomato3.show()
            tomato4.show()
            tomato8.show()
            tomato15.show()
        }

        if (counter == 3) {
            tomato9.show()
            tomato10.show()
            tomato11.show()
            tomato12.show()
            tomato13.show()
            tomato14.show()
        }
        console.log('counter' + counter)

        if (counter == 3 && player.posX >= 350 && player.posY >= 250) {
            img.resize(width, height)
            image(img, random(0, 10) - random(0, 10), random(0, 10) - random(0, 10))
            if (!mySound.isPlaying()) {
                mySound.play();
            }

        }
        if (counter == 0 && player.posX > 300 || player.posX < 200) {
            fill(0)
            rect(0, 0, 1000, 1000)
            fill(255)
            textAlign(CENTER)
            textFont(myFont, 150);
            text("GAME OVER", width / 2, height / 2)
            myTrack.stop();
            if (!lydGO.isPlaying()) {
                lydGO.play();
            }
        }
        if (counter == 1 && player.posX > 300 || player.posX < 200) {
            fill(0)
            rect(0, 0, 1000, 1000)
            fill(255)
            textAlign(CENTER)
            textFont(myFont, 150);
            text("GAME OVER", width / 2, height / 2)
            myTrack.stop();
            if (!lydGO.isPlaying()) {
                lydGO.play();
            }
        }
        if (counter == 2 && player.posX < 200) {
            fill(0)
            rect(0, 0, 1000, 1000)
            fill(255)
            textAlign(CENTER)
            textFont(myFont, 150);
            text("GAME OVER", width / 2, height / 2)
            myTrack.stop();
            if (!lydGO.isPlaying()) {
                lydGO.play();
            }
        }

        if (counter == 2 && player.posX < 400 && player.posY == 250) {
            fill(0)
            rect(0, 0, 1000, 1000)
            fill(255)
            textAlign(CENTER)
            textFont(myFont, 150);
            text("GAME OVER", width / 2, height / 2)
            myTrack.stop();
            if (!lydGO.isPlaying()) {
                lydGO.play();
            }
        }
        if (counter == 0 && player.posY > height - 50) {
            fill(0)
            rect(0, 0, 1000, 1000)
            fill(255)
            textAlign(CENTER)
            textFont(myFont, 150);
            text("GAME OVER", width / 2, height / 2)
            myTrack.stop();
            if (!lydGO.isPlaying()) {
                lydGO.play();
            }
        }
        console.log(frameCount)
    }
}

function keyPressed() {
    if (keyCode === UP_ARROW) {
        player.moveUp();
        lydP.play();
    } else if (keyCode === DOWN_ARROW) {
        player.moveDown()
        lydP.play();
    } else if (keyCode === 39) {
        player.moveRight()
        lydP.play();
    } else if (keyCode === 37) {
        player.moveLeft()
        lydP.play();
    }
}