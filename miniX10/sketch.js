
// Denne filet er en image classification, som benytter MobileNet og p5.js. Denne kode bruger et callback pattern 
// som er et stykke eksikverbar kode, der sendes som et argument til anden kode, som forventes at kaldes tilbage. 

// Laver en globalvariable som kalder på classefieren, der er trænet med MobileNet. 
let classifier;
// En global variable til at holde på det valgte billede som vi vil classificere. 
//let img;
//Laver et array der indeholder billederne, der skal kategoriseres. 
let images = [];

// Denne funktion kalder på classefier (tekst), som bliver valgt at den trænede MobileNet, samt kalder på de loadede billeder. 
function preload() {
  classifier = ml5.imageClassifier('MobileNet');
  //img = loadImage('images/1.jpeg');
  //preloader billederne i array
  images[0] = loadImage('images/1.jpeg');
  images[1] = loadImage('images/2.jpeg');
  images[2] = loadImage('images/3.jpeg');
  images[3] = loadImage('images/4.jpeg');
  images[4] = loadImage('images/5.jpeg');
  images[5] = loadImage('images/6.jpeg');
  images[6] = loadImage('images/7.jpeg');
  images[7] = loadImage('images/8.jpeg');
  images[8] = loadImage('images/9.jpeg');
  images[9] = loadImage('images/10.jpeg');
  images[10] = loadImage('images/11.jpeg');
  images[11] = loadImage('images/12.jpeg');
  images[12] = loadImage('images/13.jpeg');
  images[13] = loadImage('images/14.jpeg');
  images[14] = loadImage('images/15.jpeg');
  images[15] = loadImage('images/16.jpeg');
  images[56] = loadImage('images/18.jpeg');
  images[16] = loadImage('images/19.jpeg');
  images[17] = loadImage('images/20.jpeg');
  images[18] = loadImage('images/21.jpeg');
  images[19] = loadImage('images/22.jpeg');
  images[20] = loadImage('images/23.jpeg');
  images[21] = loadImage('images/24.jpeg');
  images[22] = loadImage('images/25.jpeg');
  images[23] = loadImage('images/26.jpeg');
  images[24] = loadImage('images/27.jpeg');
  images[25] = loadImage('images/28.jpeg');
  images[26] = loadImage('images/29.jpeg');
  images[27] = loadImage('images/30.jpeg');
  images[28] = loadImage('images/31.jpeg');
  images[29] = loadImage('images/32.jpeg');
  images[30] = loadImage('images/33.jpeg');
  images[31] = loadImage('images/34.jpeg');
  images[32] = loadImage('images/35.jpeg');
  images[33] = loadImage('images/36.jpeg');
  images[34] = loadImage('images/38.jpeg');
  images[35] = loadImage('images/39.jpeg');
  images[36] = loadImage('images/40.jpeg');
  images[37] = loadImage('images/41.jpeg');
  images[38] = loadImage('images/42.jpeg');
  images[39] = loadImage('images/43.jpeg');
  images[40] = loadImage('images/44.jpeg');
  images[41] = loadImage('images/45.jpeg');
  images[42] = loadImage('images/46.jpeg');
  images[43] = loadImage('images/47.jpeg');
  images[45] = loadImage('images/48.jpeg');
  images[46] = loadImage('images/49.jpeg');
  images[47] = loadImage('images/50.jpeg');
  images[48] = loadImage('images/51.jpeg');
  images[49] = loadImage('images/52.jpeg');
  images[50] = loadImage('images/53.jpeg');
  images[51] = loadImage('images/54.jpeg');
  images[52] = loadImage('images/56.jpeg');
  images[53] = loadImage('images/57.jpeg');
  images[54] = loadImage('images/58.jpeg');
  images[55] = loadImage('images/17.jpeg');
}

function setup() {
 //Laver lokal variabel, der vælger 1 billeder imellem de 57 billeder. Dette er smart, da det så er det billede vi refferer til resten af funktionen.
  let img = images[floor(random(57))]
   //Laver canvas
  createCanvas(500, 500);
  classifier.classify(img, gotResult);
  push();
  //Resizer billedet til 400,400
  img.resize(400, 400);
  //Indsætter billedet.
  image(img, 0, 0);
  pop();
}

// Denne funktion der køres når vi får fejl og resultater. 
function gotResult(error, results) {
  // Viser fejl og mangler i console-log. 
  if (error) {
    console.error(error);
  }
  // Resultatet er i et array, sorteret efter hvor sikker den er på resultatet/billedet. Dette bliver også vist i consolen. 
  console.log(results);
  // Disse to generere teksten. Label er hvad den kategorisere/classificere billedet som. 
  //Confidence er hvor sikker den er på det den classificer den som. 
  // Funktionen opretter et div-element som gør at man kan skrive noget. Funktionen nf bruger talformatering 
  //til at angive at sikkerheden(confidence) skal formateres med to decimaler. Det vil sige at at der kun bliver vist 0,xx
  // 0, 2 er placeringen. 
  createDiv('Label: ' + results[0].label);
  createDiv('Confidence: ' + nf(results[0].confidence, 0, 2))}