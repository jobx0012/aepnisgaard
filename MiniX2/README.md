# MiniX2: Job Ehrich
Velkommen til min **X2**

<br>
[link](https://jobx0012.gitlab.io/aepnisgaard/MiniX2/index.html)
<br>
Jeg har haft store problemer med få lavet et link til koden. Koden virker perfekt lokalt, og kan godt uploades til gitlab, men når jeg prøver at oprette en pipeline står der fejl ved både "build" og ved "test" så der kommer desværre intet link. Nedenfor er der en demonstrationsvideo af hvordan programmet virker, og ellers er man desværre nød til at kopiere koden over i sin visual studio code hvis man skal prøve at køre programmet. <br> (husk at downloade de to billederfiler også)
![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/MiniX2/Sk%C3%A6rmoptagelse%202023-02-19%20kl.%2020.56.44.mov)
<br>
I forbindelse med opgaven skal der besvares nogle spørgsmål. Disse  vil jeg besvare herunder:
 <br>
<br>
 **Hvad har jeg skabt?** <br>
Programmet hedder "what about…."  Når man åbner det kommer man ind på en forside hvor der står overskriften "what about…."  mens en masse emojier skifter i baggrunden. Disse emojier inkluderer nogle af de nye og omdiskuterede emojis som fx. Kønsneutrale emojier. I bunden af skærmen ses en slider, og i midten af skærmen blinker teksten "Move the inclusion-slider to begin" samt en pil ned på slideren.

Når man klikker på slideren dukker der en gul emoji op på skærmen. Og baggrundsbilledet skifter til et supermarked. Når man slider til step to begynder emojien at skifte hudfarve og jo længere hen man slidt jo mere "inkluderende" bliver emojiene. De får forskellige proportioner og farver og øjenform og mund. Alt sammen tilfældigt genereret inden for nogle rammer. Jo længere til høre man slider, jo hurtigere skifter emojierne også.


 <br>

**Hvordan er det lavet? Hvilke (nye/interessante) syntakser bliver brugt?** <br>
Programmet er bygget op omkring en slider. Slideren har 5 trin. Trinenne er knyttet til 5 seperate if-statemens, så når slideren for eksempel er på først trin vil den give tallet 1. (det er ikke et tal brugeren kan se, men koden bruger det). Så har jeg en kode der siger. `If (val==1){koden til den første emoji}` og så fremdeles med de 5 andre trin på slideren. Jeg har også indsat to billeder. Et png af en gul pil hvor jeg har brugt `tint(xxx)` til at sænke opaciteten.
Slideren har 5 tal og hvert tal triggerer et if-statement. Med mange forskellige randomiserede faktorer. Jeg har også brugt ´push´ og ´pop´ til at begrænse nogle af fill, linjetykkelserne, alpha-værdierne og textstyles til specifikke dele af koden.
Jeg har også brugt `function mousePressed()` til at man isolere startskærmen fra resten af programmet, så når mousePressed er true, kommer man videre til anden del af programmet.
	
`random()`er et af de bærene elementer i koden, da alle emojierne er tilfældigt genereret. Både hudfarven, formen, opaciteten af øjne og mund, samt typen og formen af øjne og mund er alt sammen tilfældigt genereret inden for nogle rammer. Jeg stødte på problemer med at skifte mellem x antal specifikke hudfarver, så jeg var nød til at lave en array med 5 forskellige farver i hex-format, og få den til at vælge tilfældigt mellem disse farver.
	
Jeg kæmpede meget med at få teksten på startskærmen til at blinke uafhængigt af framerate, men i stedet ud fra hvor lang tid der var gået i millisekunder. Jeg fandt til sidst en guide til hvordan man gjorde, men det var ret kompliceret og jeg forstår ikke fuldt ud alle dele, men jeg tilpassede det til mit program og fik det til at virke rigtigt godt. Det er al den kode med `toggleXxx`



<br> <br>
 **Hvad får det dig til at tænke på eller føle?** <br>
Koden får mig til at reflektere over whatabousismen og hvor det er bedst at sætte grænsen for hvor meget det er muligt at inkludere. Når man inkluderer mere og mere, bliver det nemlig også mere tydeligt hvad der ikke bliver inkluderet og det kan gøre det oprindelige problem større. Jo mere man slider til højre jo større ændringer sker der og jo hurtigere bliver der også genereret emojis, så på en måde bliver det mere og mere hektisk, og på en måde får man som bruger af programmet bare lyst til scrolle tilbage til den mere simple gule emoji, hvor det hele er mere simpelt og nemt.
	
Koden er for mig også et klart eksempel på at kode ikke er neutralt. For selvom det aldrig står klart om koden er pro-whataboutisme eller anti, og den i sin essens bare sætter tanker i gang, er der flere indikatorer på at koden ikke er neutral. Den "neurale" emoji på step-1 af slideren af nemlig lille og statisk, hvorimod jo længere man slider til højre jo højere bliver frameraten på programmet og dermed bliver emojierne genereret hurtigere, og det giver altså en kunstig/tilført følelse af at det bliver hektisk, selvom det slet ikke ville være ligeså hektisk hvis frameraten var meget lavere. Altså er der bevidst gjort brug af virkemidler for at illustrere en pointe. Øjnenes placering på de tilfældigt genererede emojies er også uanhægige af hinanden, og kan derfor sidde helt skævt i ansigtet (f.eks. Kan det ene øje have en meget højere y-værdi end det andet), og øjnene har også forskellige størrelse (Højre fra venstre) og dette er også med til at give det hele et mere kaotisk udtryk. Koden kan altså altså i lille grad, men stadig vigtigt at lægge mærke til, have tildens til at give brugeren følelsen af at det er meget mindre kompliceret uden alle de forskellige typer inkluderende emojis, og at hvis man bare tager den gule så slipper man for alt "bøvlet" og så træder man ingen over tæerne.
	
Til sidst vil jeg italesætte valget af baggrund. Jeg synes det var et flot billede med mange ledende linjer som pegede imod emojien. Det så også bedre ud end en monokrom baggrund. Der er ikke andre grunde til valget, men måske du synes det gør noget ved udtrykket?

<br>
<br>
Chao

*- Job Ehrich* <br>
<br>

────────────────────────────────────────
────────────────────────────────────────
───────────████──███────────────────────
──────────█████─████────────────────────
────────███───███───████──███───────────
────────███───███───██████████──────────
────────███─────███───████──██──────────
─────────████───████───███──██──────────
──────────███─────██────██──██──────────
──────██████████────██──██──██──────────
─────████████████───██──██──██──────────
────███────────███──██──██──██──────────
────███─████───███──██──██──██──────────
───████─█████───██──██──██──██──────────
───██████───██──────██──────██──────────
─████████───██──────██─────███──────────
─██────██───██─────────────███──────────
─██─────███─██─────────────███──────────
─████───██████─────────────███──────────
───██───█████──────────────███──────────
────███──███───────────────███──────────
────███────────────────────███──────────
────███───────────────────███───────────
─────████────────────────███────────────
──────███────────────────███────────────
────────███─────────────███─────────────
────────████────────────██──────────────
──────────███───────────██──────────────
──────────████████████████──────────────
──────────████████████████──────────────
────────────────────────────────────────
────────────────────────────────────────

