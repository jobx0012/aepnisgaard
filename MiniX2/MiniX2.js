//definerer nogle variabler 
let showText = false; //definerer showText og giver den boolean værdi "falsk"
let klik = false; //forbundet til mousepressed funktionen + se ovenstående
let lastToggleTime = 0;
let slider; //definerer slideren
let myColors = ["#fcffcc", "#ffe6a8", "#ffc942", "#a36903", "#301f00"];let randCol; //de forskellige hudfarvetyper, jeg kunne ikke få det til at fungere med rgb

//Setupkode
function setup() { //setup
    img1 = loadImage('sm.jpg')
    img = loadImage('a.png'); 
    slider = createSlider(1, 5,1,1); //slideren går fra 1-5, starter i 1 og bevæger sig 1 klik ad gangen)
    slider.position(windowWidth/2-150,windowHeight/2+300); //sliderens position
    slider.style('width', '300px'); //sliderens længde
    createCanvas(windowWidth, windowHeight); //canvas
    textAlign(CENTER,CENTER); //bruger width og height til at placere teksten så det var lettere at centrere den med centerAlign
  textSize(80); //tekst størrelse
  frameRate(10) //startframerate, men den ændrer sig alt efter sliderens position
}

//Drawkoden
function draw() {
    let val = slider.value();      //laver en variabel som har den værdi slideren har, altså et tal mellem 1 og 5


    //først opsætter jeg startskærmen, altså alt der bliver vist før der bliver klikket første gang
  if (klik==false) { //forbundet til mousePressed funktionen
    background(0); // sort baggrund
    fill(255,255,0,100,)
    stroke(255,255,0)
    let e = floor(random(1,13))
    let x = 0
    textStyle(BOLD) //fed tekst
    push()
    textSize(1500) //kæmpe emoji
    stroke(255,255,0,0) //lav alpha
    if (e==1){text("🧕🏻", windowWidth/2+x,windowHeight/2);} //i midten af skærmen
    if (e==2){text("🥷🏿", windowWidth/2+x,windowHeight/2);}
    if (e==3){text("👼🏿", windowWidth/2+x,windowHeight/2);}
    if (e==4){text("🧏", windowWidth/2+x,windowHeight/2);}
    if (e==5){text("👩🏻‍❤️‍👩🏻", windowWidth/2+x,windowHeight/2);}
    if (e==6){text("😵", windowWidth/2+x,windowHeight/2);}
    if (e==7){text("🍆", windowWidth/2+x,windowHeight/2);}
    if (e==8){text("👯‍♂️", windowWidth/2+x,windowHeight/2);}
    if (e==9){text("🧑🏻‍🎄", windowWidth/2+x,windowHeight/2);}
    if (e==10){text("🖕🏽", windowWidth/2+x,windowHeight/2);}
    if (e==11){text("👳🏼‍♀️", windowWidth/2+x,windowHeight/2);}
    if (e==12){text("👮🏻‍♀️", windowWidth/2+x,windowHeight/2);}
    pop()
    text("What about ....", windowWidth/2,windowHeight/8); //tekst
  let timeSinceToggle = millis() - lastToggleTime; //tidsinterval får teksten til at bliknke:
  if (timeSinceToggle > 600) { //hvert 600. tidsenhed
    showText = !showText; //også med til at få teksten til at blinke
    lastToggleTime = millis(); //vælger at tidsneheden skal være i millisekunder
  }
  
  if (showText) {
    tint(100) //lavere opacitet for billeder
    img.resize(60, 60); //gør billedet mindre
    image(img, windowWidth/2-30, 550); //indsætter billede af en gul pil
    fill(255,255,0,50); //ændrer farve til gul og alpha til 50
    textStyle(ITALIC) //kursiv
    text("Move the inclusion-slider to begin", width/2, height/2); //den tekst der blinker


//når der bliver klikket med musen kommer vi herind
  }} else { 
    stroke(0) //linjetykkelse (0 er vidst det samme som 1. da 1 er minimum. Hvis jeg ingen linje ville have haft, skulle jeg have skrevet noStroke())
    background(255,255,0) //baggrund gul
    tint(200) //lavere opacitet for billeder
    img1.resize(width,height+110); //gør billedet mindre
    image(img1, 0,0); //indsætter billede af en gul pil

    //der kommer løbende 5 forskellige if-statements som starter med if(val==x), hvor x er alle hele tal fra 1 til 5
    //det første kommer her
if (val==5) {
    frameRate(5) //framerate 5
    push() //ikke sikker på at mit push/pop har nogen effekt, men gjorde det for en sikkerhedsskyld for at tingene ikke skulle interferere med hinanden
  let a1 = floor(random(1,3)) //skaber en variabel a1 som er et tal mellem 1 og 3, men da det er decimaltal man får ud af det skriver jeg "floor" så det kun er hele tal jeg får ud af det. og den kan ikke runde op så jeg får altså tallene 1 og 2 ud, tilfældigt
   let a2 = floor(random(1,4)) //det samme bare med 1,2 og 3.
  randCol=random(myColors.length); //hænger sammen med farverne jeg definerede tidligere. for at ændre hudfarve tilfædligt mellem 5 specifikke udvalgte farve
  randCol=floor(randCol); //samme
  fill(myColors[randCol]); //indsætter hudfarverne som fill
  ellipse(width / 2, height / 2, 500+random(300),500+random(300)) //
  fill(0,0,0) //ændrer fill til sort

  //laver en tilfældig genereret mund ud fra arc
  if(a2==1){
  arc(width/2+-random(20), height/2+200+-random(20), 120+-random(100), 120+-random(100), +-random(0), PI/+-random(4),CHORD )}
//en helt normal glad smileymund
  if (a2==2){arc(width/2, height/2+95, 120, 120, 0, PI );}
    //tilfældig genereret mund ud fra arc men uden "CHORD" og med "noFill()", så den bliver "tynd"
  if (a2==3){
    noFill()
    push()
    strokeWeight(random(5))
    arc(width/2+-random(20), height/2+200+-random(20), 120+-random(100), 120+-random(100), +-random(0), PI )}
    pop()

    //øjnene
fill(0,0,0,random(0,500)) //variende alpha men sorte
//tilfældigt genererede ellipser i størrelse og placering  
if(a1==1){
 ellipse(width/2-115+-random(20), height / 2-30+-random(20), 150+-random(100),150+-random(100)),
 ellipse(width / 2+115+-random(20), height / 2-30+-random(20), 150+-random(100),150+-random(100))
}
  //tilfældigt genererede trekanter
else [
    triangle(width/2-115+-random(20), height / 2-30+-random(20),width/2-115+-random(20)-50, height / 2-30+-random(20),width/2-115+-random(20), height / 2-30+-random(20)-40),
    triangle(width/2+115+-random(20), height / 2-30+-random(20),width/2+115+-random(20)+50, height / 2-30+-random(20),width/2+115+-random(20), height / 2-30+-random(20)+40)
]
pop ()}


//meget af det samme som val==5, men med lindre mindre "vild" variation
if (val==4) {
    frameRate(4)
    push()
    let a2 = floor(random(1,4))
    randCol=random(myColors.length);
    randCol=floor(randCol);
    fill(myColors[randCol]);
    ellipse(width / 2, height / 2, 500+random(300),500+random(300))
    fill(0,0,0)
    if(a2==1){
    arc(width/2+-random(20), height/2+200+-random(20), 120+-random(100), 120+-random(100), +-random(0), PI/+-random(4),CHORD )}
    if (a2==2){arc(width/2, height/2+95, 120, 120, 0, PI );}
    if (a2==3){
      noFill()
      push()
      strokeWeight(random(5))
      arc(width/2+-random(20), height/2+200+-random(20), 120+-random(100), 120+-random(100), +-random(0), PI )}
      pop()
      fill(0,0,0,random(0,500))    
   ellipse(width/2-115, height / 2-30, 100+-random(50),100+-random(50))
   ellipse(width/2+115,height/2-30, 100+-random(50),100+-random(50))
  pop ()}


  //endnu mindre vild variation
  if (val==3) {
    frameRate(3)
    push()
    let a2 = floor(random(2,4))
    randCol=random(myColors.length);
    randCol=floor(randCol);
    fill(myColors[randCol]);
    ellipse(width / 2, height / 2, 500+random(150),500+random(150))
    fill(0,0,0)
    if(a2==1){
    arc(width/2+-random(20), height/2+200+-random(20), 120+-random(100), 120+-random(100), +-random(0), PI/+-random(4),CHORD )}
    if (a2==2){arc(width/2, height/2+95, 120, 120, 0, PI );}
    if (a2==3){
      noFill()
      push()
      strokeWeight(random(5))
      arc(width/2+-random(20), height/2+200+-random(20), 120+-random(100), 120+-random(100), +-random(0), PI )}
      pop()
      fill(0,0,0,random(0,500))    
   ellipse(width/2-115, height / 2-30, 70,70)
   ellipse(width/2+115,height/2-30, 70,70)
  pop ()}


  //her er det kun hudfarven der skifter og formen på ansigtet skifter også en my
  if (val==2) {
    frameRate(2)
    push()
    let a2 = floor(random(1,4))
    randCol=random(myColors.length);
    randCol=floor(randCol);
    fill(myColors[randCol]);
    ellipse(width / 2, height / 2, 500+random(50),500+random(50))
    fill(0,0,0)
    ellipse(width / 2-115, height / 2-30, 70,70)
    ellipse(width / 2+115, height / 2-30, 70,70)
    arc(width/2, height/2+95, 120, 120, 0, PI );
  pop ()}


// en klassisk gul smilende smiley
if (val==1){
push()
fill(255,255,0)
ellipse(width / 2, height / 2, 500,500)
fill(0)
ellipse(width / 2-115, height / 2-30, 70,70)
ellipse(width / 2+115, height / 2-30, 70,70)
arc(width/2, height/2+95, 120, 120, 0, PI );
} } }

//musseklik funktionen
    function mousePressed() {
      
        klik = true;
     
      }



  