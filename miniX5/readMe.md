# MiniX5: Job Ehrich
<br>
Velkommen til min miniX5
<br>
[link til program](https://jobx0012.gitlab.io/aepnisgaard/miniX5/indexx.html)
<br>

![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/miniX5/Sk%C3%A6rmbillede%202023-03-19%20kl.%2018.02.53.png)
<br>
![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/miniX5/Sk%C3%A6rmbillede%202023-03-19%20kl.%2018.03.14.png)
<br>
![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/miniX5/Sk%C3%A6rmbillede%202023-03-19%20kl.%2018.03.34.png)
<br><br>

### Aarhus universitet; strik og kod
Mit program er en generativ plakat til Aarhus universitets arrangement strik og kod. Jeg har bare opstillet et fiktivt scenarie, og har altså ikke rigtigt lavet den til arrangementet, men Au har et arrangement der hedder strik og Kod, og jeg synes plakaten, af indlysende grunde, passer godt til dette arrangement.
<br><br>
programmet består af en masse statiske objekter ( de fremstår  statiske for brugeren), i form af teksten, baggrunden og hænderne der holder strikkepinde. og så er det strik der kommer ud af hænderne den generative statiske del af programmet. Jeg synes kapitlet med generativ kode var mega spændende og jer ser så meget potentiale i det, især fordi det udnytter det kæmpe potentiale kode har som kreativt værktøj, som man ikke får på nogen andre udtryksformer; Hele sammenspillet mellem det man har kontrol over og det man ikke har kontrol over, og som skaber bliver man konstant overrasket over hvad der sker i programmet. For selvom man hele tiden har en idé om hvad man gør, f.eks. hvis man ændrer en værdi fra 5 til 10, så kan man alligevel blive overrasket over hvad programmet outputter. Man sætter ligesom rammerne for programmet og så skaber det selv en masse kunst, som ALDRIG er ens to gange i træk!
<br><br>
Jeg har fået inspiration 3 forskellige steder fra til min kode. Først og fremmest har jeg tænkt meget på Jaqquard loom, som vi har hørt om flere gange nu, og hele ideen med at kode i sin grundessens er 1'ere og 0'ere, og at man allerede før computeren kunne arbejde med denne logik. Jeg ønskede at arbejde med dette, og få brugerent til at reflektere over dette ved at lave et stof-strikke-mønster på en computer.
<br><br>
Mit program er også inspireret af 10PRINT programmet kendt fra C64. Jeg synes det var så fascinerede at noget som simpelt som en skråstreg i to retninger kunne skabe så flot og fængende grafik, og at man ved at ændre på småting kunne ændre udtrykket fuldstændigt. Jeg har taget udgangspunkt i koden fra 10-PRINT i dette program, og har brugt den som stor inspiration, men samtidigt ændret den gevaldigt, og gjort det meget mere kompliceret. Men grundessensen er den samme, hvilket man også kan se hvis man sammenligner de to koder.
<br><br>
Til sidst har jeg også taget inspiration fra Stig Møller Hansen, en dansk kunstner der også arbejder med kode. Han arbejder med allerede eksisterende plakater med forskellige mønstre, og tager så og skaber generativ kode inde i disse plakater. Jeg synes det er genianlt og virkeligt flot, og jeg ser så meget fremtidig potentiale i denne idé. Man kan se hans kunst på instagram, hvor der bl.a. også er en kode han har lavet til en norsk strikkefestival.
<br>
Min arbejdsprocess har været lang, da jeg egenligt startede med en helt anden plakat som han ses her:

![](https://gitlab.com/jobx0012/aepnisgaard/-/raw/main/miniX5/Sk%C3%A6rmoptagelse%202023-03-19%20kl.%2018.04.49.mov)
<br>
[link til mit oprindelige program](https://jobx0012.gitlab.io/aepnisgaard/miniX5/index.html)
<br>
<br>
Dette program tog 3 gange så lang tid at kode, men der er et stort problem: det er ikke generativt nok. De rammer programmet ligesom har at arbejde med er så smalle at selvom programmet bygger på en masse noise-funtioner og ligner det lever sit eget liv, er det i sin essens meget det samme der sker og man bliver ikke rigtigt overrasket over noget. Det var fedt at arbejde med noise functionen der der mega naturlig ud, som om den lever sit eget liv. Det kan bl.a. ses på figurerne i denne plakat, som ser nærmest levende/organiske ud.
<br>
Men det har fået mig til at reflektere yderligere over hvad generativ kode er. Det handler nemlig om at sætte nogle regler, som skaber værket, og som ER værket, i stedet for at bestemme det meste på forhånd. Man kan sige, at hvis man præcis ved hvordan slutproduktet skal se ud, så er det ikke generativ kode man skal arbejde med.
<br>
<br>
Koden er bygget op omkring en `noise()`function med og et `if-statement` med `%2`. Jeg satte `floor`rundt om noise så jeg kun arbejdede med hele tal, og hver gang de tilfældigt genererede tal kunne deles med 2, var statementet sandt og ellers var det falsk. Hvis det var sandt blev firkanterne hvide, og ellers blev de blå(samme farve som baggrunden). Det er egenligt bare dette som er kernen er koden, og fordi det er så simpelt, men klare regler, skaber det flotte mønstre. For at gøre det meget pænere spejlede jeg koden på midten, så det ligner rigtigt mønstre hele vejen igennem, og så har jeg begrænset koden inde for et område, og fået den til at starte forfra, hver gang y-værdier på firkanterne overskrider en hvis grænse. Det sværeste ved det hele var faktisk at lave "starten" på garnet, da det var en trekanten start. Det endte jeg med at lave med en usynlig trekant samen en masse if-statements som gør koden meget mere lang og besværlig. Måske der er en nemmere måde at gøre det på???
<br>
<br>
slut
