let x = 0;
let y = 280;
let xoff = 100
let r
let spacing;
let vary = 3.5

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(150, 200, 255);
    frameRate(60)
    push()
    fill(0, 100)
    noStroke()
    rect(0, 0, width / 4, height)
    rect(width / 4 * 3, 0, width / 4, height)
    pop()
    push()
    noStroke()
    fill(255, 100)
    textAlign(CENTER)
    textSize(30)
    text('Aarhus universitet:', width / 2 - 150, 50)
    pop()

    push()
    strokeWeight(2)
    fill(255)
    textAlign(CENTER)
    textSize(10)

    text('19/4', width / 4 + 30, height - 20)
    text('Adorno, Katrinebjerg', width / 3 * 2 + 40, height - 20)
    pop()


    img = loadImage('strik.png');
}

function draw() {
    push()
    noStroke()
    fill(255)
    textAlign(CENTER)
    textSize(120)
    text('Strik & kod', width / 2, 150)
    pop()
    push()
    noStroke()
    fill(150, 200, 255)
    beginShape();
    vertex(width / 2, 286)
    vertex(370, 400)
    vertex(700, 160)
    vertex(1060, 400)
    vertex(width / 2, 285)
    endShape(CLOSE);
    pop()
    push()
    scale(0.5)

    translate(width / 2, 300)
    //background(150, 200, 255,4);

    push()
    //img.resize(width+20,height*2) 
    img.resize(width, height * 2)
    //image(img,r/4-20,r/4)
    image(img, 0, 0)
    pop()
    if (r < 25) {
        spacing = 10;
    }
    else if (r >= 25 && r > 40) {
        spacing = 15
    }
    else if (r >= 40 && r > 60) {
        spacing = 20
    }
    else {
        spacing = 25
    }
    r = floor(noise(xoff) * 100)
    stroke(255);
    strokeWeight(0)
    if (r % 2) {
        fill(255)
    } else {
        fill(150, 200, 255)
    }
    push()
    scale(-1, 1);
    rect(width / 2 + x - width, y, spacing, spacing);
    pop()
    rect(width / 2 + x, y, spacing, spacing);
    x = x + spacing;
    if (x > width / 2 - width / vary && y > 350) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 2 && y < 290) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 2.5 && y < 300) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 2.5 && y < 310) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 2.5 && y < 320) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 3 && y < 330) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 3 && y < 340) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 3 && y < 350) {
        x = 0
        y = y + spacing;
    }
    else if
        (x > width / 2 - width / 3 && y < 355) {
        x = 0
        y = y + spacing;
    }
    xoff += 0.01
    console.log(r)
    if (y > height * 1.6) {
        y = 280
        push()
        fill(150, 200, 255);
        rect(300, 280, width / 1.8, height * 2)
        pop()
    }
}