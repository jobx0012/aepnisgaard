let xoff = 0.1
let xxoff = 1000000
let yoff = 100
let zoff = 5000
let yyoff = 400
let rand1
let rand2
let rand3
let rand4
let rand5
let randOffset = Math.random() * 200
let randOffset1 = Math.random() * 50
let randOffset2 = Math.random() * 50
let randOffset3 = Math.random() * 50
let randOffset4 = Math.random() * 50


function setup() {
    createCanvas(windowWidth, windowHeight)
    angleMode(DEGREES)

}


function draw() {
    let rand1 = noise(xoff) * 100
    let rand2 = noise(yoff) * 100
    let rand3 = noise(zoff) * 100
    let rand4 = noise(xxoff) * 100
    let rand5  =noise(yyoff)*100
    background(0)
    fill(255)
    fill(255/*rand1*1.5,255,rand2*2*/)
    rect(width / 4, 0, width / 4 * 2, height)
    fill(180,100)
    strokeWeight(2)
    rect(width / 5*1.5 , height/9, width / 5*2 , height/1.17)
    fill(rand1 * 3 - 90, 200-rand5, rand2 * 2 - 140)
    push()
    stroke(0)
    fill(0)
    textAlign(CENTER)
    textSize(60)
    text('Strynø designmesse',width/2,60)
    pop()
    push()
    strokeWeight(1)
    stroke(0)
    noFill()
    textAlign(CENTER)
    textSize(10)
    text('27. april 1998',width/2+250,height-10)
    pop()

    if (rand4 <= 50 && rand4 > 30) {
        translate(width / 2, height / 2)
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {
                push()
                if (j+i %2) {
                    fill(rand1 * 3 - 90, 200-rand5*3, rand2 * 2 - 140)
                }
                    if (j*i %2) {
                    fill(rand1 * 3 - 90, 200-rand5*2, rand2 * 2 - 140)
                }
                if ((j*i+5) %2) {
                    fill(rand1 * 3 - 90, 200-rand5, rand2 * 2 - 140)
                }
                rotate(0)
                scale(0.9)
                translate(150 * i, 180 * j)
                figur(-530, -570, randOffset4, 2)
                pop()
            }
        }
    }
    else if (rand4 <= 30 && rand4 > 0) {
        translate(width / 2, height / 2)
        for (let i = 0; i < 7 ; i++) {
            for (let j = 0; j < 6; j++) {
                push()
                  if (j*i %2) {
                    fill(rand1 * 3 - 90, 200-rand5*2, rand2 * 2 - 140)
                }
                if ((j*i+5) %2) {
                    fill(rand1 * 3 - 90, 200-rand5*1.3, rand2 * 2 - 140)
                }
                scale(0.5)
                rotate(0)
                translate(160 * i, 220 * j)
                figur(-1020, -1100, randOffset4, 2)
                pop()
            }
        }
    }
    else if (rand4 <= 60 && rand4 > 50) {
        translate(width / 2, height / 2)
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                push()
                  if (j*i %2) {
                    fill(rand1 * 3 - 90, 200-rand5*2, rand2 * 2 - 140)
                }
                if ((j*i+5) %2) {
                    fill(rand1 * 3 - 90, 200-rand5*1.3, rand2 * 2 - 140)
                }
                scale(1.1)
                rotate(0)
                translate(170 * i, 190   * j)
                figur(-420, -420, randOffset4, 2)
                pop()
            }
        }
    }
    else if (rand4 < 70 && rand4 > 60) {
        translate(width / 2, height / 2)
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < 2; j++) {
                push()
                  if (j*i %2) {
                    fill(rand1 * 3 - 90, 200-rand5*2, rand2 * 2 - 140)
                }
                if ((j*i+5) %2) {
                    fill(rand1 * 3 - 90, 200-rand5*1.3, rand2 * 2 - 140)
                }
                scale(1.7)
                rotate(0)
                translate(160 * i, 180 * j)
                figur(-240, -270, randOffset4, 2)
                pop()
            }
        }
    }

    else {
        push()
        translate(width / 2 - 200, height / 2 - 200)
        scale(1.6)
        figur(50, 50, randOffset2, 1)
        pop()
    }


    xoff += 0.01
    yoff += 0.007
    zoff += 0.008
    xxoff += 0.005
    yyoff += 0.01
    console.log(rand5)
}


function figur(x, y, z, a) {
    let rand1 = noise(xoff) * 200
    let rand2 = noise(yoff) * 200
    let rand3 = noise(zoff) * 200
    noStroke()
    beginShape();
    curveVertex((x - 100 + rand2 + z) / a, (z + y - 10 + rand3) / a)
    curveVertex((x - 100 + rand2 + z) / a, (z + y - 10 + rand3) / a)
    curveVertex((x - 120 + rand2 + z) / a, (z + y - 150 + rand2) / a)
    curveVertex((x + 10 + rand2 + z) / a, (z + y + -120 + rand1) / a)
    curveVertex((x + 80 + rand2) / a, (y + -40 + rand1) / a)
    curveVertex((x + 60 + rand1 + z) / a, (y + 50 + rand2 + z) / a)
    curveVertex((x - 40 + rand3) / a, (y + 120 + rand1 + z) / a)
    curveVertex((x - 140 + rand1 + z) / a, (y + 160 + rand3) / a)
    curveVertex((x - 100 + rand2 + z) / a, (z + y - 10 + rand3) / a)
    endShape(CLOSE);

}
