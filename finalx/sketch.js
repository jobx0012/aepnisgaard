
let myJson; //Referer til vores jason fil. Hvor alt teksten står i. 
let pn; //Variabl der definere sidenummeret 

let videox = 120; //Endelige x-værdi på anbefalinger
let videoy = 90; // endelig y-væri på anbefalinger

let midx = 500; // En global værdi som hele tiden kan ændres afhæning af placering - refere til x-aksen 
let midy = 300; // En global værdi som hele tiden kan ændres afhæning af placering - refere til y-aksen

let midx1 = 600; // Dette ændre på teksten format - det vil sige om det er langt eller smalt på hhv. x og y aksen. 
let midy1 = 300;

let personax = 500; // Dette ændre på teksten format - det vil sige om det er langt eller smalt på hhv. x og y aksen. 
let personay = 200;

let kategorix = 200
let kategoriy = 20

let img1;
let img2;
let img3;
let img4;
let img5;
let img6;
let img7;
let img8;
let img9;
let img10;
let img11;
let img12;
let img13;
let img14;
let img15;
let img16;

let ung1 = [];
let ung2 = [];
let ung3 = [];

let mand1 = [];
let mand2 = [];
let mand3 = [];

let kvinde1 = [];
let kvinde2 = [];
let kvinde3 = [];

let gravid1 = [];
let gravid2 = [];
let gravid3 = [];


let noAlgo1 = [];
let noAlgo2 = [];
let noAlgo3 = [];


let picker;
let enterPress = false;
let side = 0
let loopDone = false;
let komFra;

let blinkInterval = 100; //får koden til at blinke i 50 ms



function preload() { // Her loades vores json fil. 
    myJson = loadJSON("jam.json");
    img4 = loadImage('images/gravid.png')
    img3 = loadImage('images/queer.png')
    img1 = loadImage('images/man.png')
    img2 = loadImage('images/old.png')
    img5 = loadImage('images/fram1.png')
    img6 = loadImage('images/fram2.png')
    img7 = loadImage('images/fram3.png')
    img8 = loadImage('images/fram4.png')
    img9 = loadImage('images/text.png')
    img10 = loadImage('images/homepage.png')
    img11 = loadImage('images/baggrund.png')
    img12 = loadImage('images/cp.png')
    img13 = loadImage('images/no.png')
    img14 = loadImage('images/ur.png')
    img15 = loadImage('images/job1.png')
    img16 = loadImage('images/job2.png')
    img17 = loadImage('images/job3.png')

    for (let i = 0; i < 8; i++) {
        ung1[i] = loadImage('images/personer/ung/shopping/' + i + '.png');
        ung2[i] = loadImage('images/personer/ung/mad/' + i + '.png');
        ung3[i] = loadImage('images/personer/ung/begivenheder/' + i + '.png');
        mand1[i] = loadImage('images/personer/mand/shopping/' + i + '.png');
        mand2[i] = loadImage('images/personer/mand/mad/' + i + '.png');
        mand3[i] = loadImage('images/personer/mand/begivenheder/' + i + '.png');
        kvinde1[i] = loadImage('images/personer/kvinde/shopping/' + i + '.png');
        kvinde2[i] = loadImage('images/personer/kvinde/mad/' + i + '.png');
        kvinde3[i] = loadImage('images/personer/kvinde/begivenheder/' + i + '.png');
        gravid1[i] = loadImage('images/personer/gravid/shopping/' + i + '.png');
        gravid2[i] = loadImage('images/personer/gravid/mad/' + i + '.png');
        gravid3[i] = loadImage('images/personer/gravid/begivenheder/' + i + '.png');
    }


    for (let i = 0; i < 38; i++) {
        noAlgo1[i] = loadImage('images/noAlgo/shopping/' + i + '.png');
        noAlgo2[i] = loadImage('images/noAlgo/mad/' + i + '.png');
        noAlgo3[i] = loadImage('images/noAlgo/begivenheder/' + i + '.png');
    }





}


function setup() {
    createCanvas(1000, 600);
    pn = 1 // Denne vil sige at vi starter på side 1 i programmet. 
    picker = 1 // Denne vil sige at vi starter på reCommander nr. 1 på side 3 
}

function draw() {
    if (pn == 4 || pn == 5 || pn == 6 || pn == 7 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42) {
        loop();
        image(img14, 0, 0, 1000, 600)
        console.log('DET VIRKER')

    }
    if (pn == 12) { // Denne side referer til startskærmen  
        image(img15, 0, 0, 1000, 600)
        if (frameCount % blinkInterval < blinkInterval / 2) {
            push()
            noStroke()
            fill(255)
            textFont('Futura');
            textSize(15);
            textAlign(CENTER, CENTER);
            text("{ Press anywhere to continue }", midx, midy + 240);
            pop()
        }
    }
    if (pn == 13) { // Denne side referer til startskærmen  
        image(img16, 0, 0, 1000, 600)
        if (frameCount % blinkInterval < blinkInterval / 2) {
            push()
            noStroke()
            fill(255)
            textFont('Futura');
            textSize(15);
            textAlign(CENTER, CENTER);
            text("{ Press anywhere to continue }", midx, midy + 140);
            pop()
        }
    }
    if (pn == 14) { // Denne side referer til startskærmen  
        image(img17, 0, 0, 1000, 600)
        if (frameCount % blinkInterval < blinkInterval / 2) {
            push()
            noStroke()
            fill(255)
            textFont('Futura');
            textSize(15);
            textAlign(CENTER, CENTER);
            text("{ Press anywhere to begin }", midx, midy + 140);
            pop()
        }
    }
    if (picker > 4) {
        picker = 1
    }
    if (picker < 1) {
        picker = 4
    }
    console.log('side', side)
    console.log('pn', pn)
    if (pn == 1) { // Denne side referer til startskærmen  
        background(0);
        push()
        textAlign(CENTER, CENTER);
        textFont('Futura');
        textSize(15);
        stroke(255, 255, 255)
        fill(255);
        text(myJson.startskærm, 200, 150, midx1, midy1);
        pop()
        if (frameCount % blinkInterval < blinkInterval / 2) {
            push()
            noStroke()
            fill(255)
            textFont('Futura');
            textSize(15);
            textAlign(CENTER, CENTER);
            text("{ Press anywhere to continue }", midx, midy + 140);
            pop()
        }
    }


    if (pn == 2) { // Denne side referer til homepage 
        background(153, 204, 255);
        image(img10, 0, 0, 1000, 600)
        if (frameCount % blinkInterval < blinkInterval / 2) {
            push()
            textFont('Futura');
            textSize(25);
            textAlign(CENTER, CENTER);
            text("{ Press anywhere to continue }", midx, midy + 80);
            pop()
        }
    }
    if (pn == 3) { // Denne side referer til udvalgssiden  
        image(img11, 0, 0, 1000, 600)
        push()
        textFont('Futura');
        textSize(15);
        textAlign(CENTER, CENTER);
        text("Move with ARROWS, press ENTER to confirm", midx + 30, midy * 2 - 50);
        pop()
        if (picker == 1) {
            image(img8, 0, 0, 1000, 600)
        }
        else if (picker == 2) {
            image(img7, 0, 0, 1000, 600)
        }
        else if (picker == 3) {
            image(img6, 0, 0, 1000, 600)
        }
        else if (picker == 4) {
            image(img5, 0, 0, 1000, 600)
        }

        image(img1, 0, 0, 1000, 600)
        image(img2, 0, 0, 1000, 600)
        image(img3, 0, 0, 1000, 600)
        image(img4, 0, 0, 1000, 600)
        image(img9, 0, 0, 1000, 600)
    }
    if (pn == 3 && picker == 1 && enterPress == true) {
        side = 1
        image(img11, 0, 0, 1000, 600)
        image(img8, 0, 0, 1000, 600)
        image(img1, 0, 0, 1000, 600)
        push()
        textFont('Futura');
        textSize(15);
        textAlign(CENTER, CENTER);
        text("Press ENTER to see the algorithm", midx + 30, midy * 2 - 50);
        pop()

        //Denne tekst passer til den unge mand
        textAlign(LEFT);
        textFont('Futura');
        textSize(15);
        fill(0);
        stroke(0.02);
        text(myJson.persona1, 400, 225, personax, personay);
    }

    if (pn == 3 && picker == 2 && enterPress == true) {
        side = 2
        image(img11, 0, 0, 1000, 600)
        image(img7, -175, 0, 1000, 600)
        image(img2, -175, 0, 1000, 600)

        push()
        textFont('Futura');
        textSize(15);
        textAlign(CENTER, CENTER);
        text("Press ENTER to see the algorithm", midx + 30, midy * 2 - 50);
        pop()

        // Denne tekst passer til den gamle mand 
        textAlign(LEFT);
        textFont('Futura');
        textSize(15);
        fill(0);
        stroke(0.02);
        text(myJson.persona2, 400, 225, personax, personay);
    }

    if (pn == 3 && picker == 3 && enterPress == true) {
        side = 3
        image(img11, 0, 0, 1000, 600)
        image(img6, -370, 0, 1000, 600)
        image(img3, -370, 0, 1000, 600)

        push()
        textFont('Futura');
        textSize(15);
        textAlign(CENTER, CENTER);
        text("Press ENTER to see the algorithm", midx + 30, midy * 2 - 50);
        pop()

        // Denne tekst passer til queer kvinden
        textAlign(LEFT);
        textFont('Futura');
        textSize(15);
        fill(0);
        stroke(0.02);
        text(myJson.persona3, 400, 225, personax, personay);
    }


    if (pn == 3 && picker == 4 && enterPress == true) {
        side = 4
        image(img11, 0, 0, 1000, 600)
        image(img5, -530, 0, 1000, 600)
        image(img4, -530, 0, 1000, 600)

        push()
        textFont('Futura');
        textSize(15);
        textAlign(CENTER, CENTER);
        text("Press ENTER to see the algorithm", midx + 30, midy * 2 - 50);
        pop()

        // Denne tekst passer til den gravide kvinde
        textAlign(LEFT);
        textFont('Futura');
        textSize(15);
        fill(0);
        stroke(0.02);
        text(myJson.persona4, 400, 225, personax, personay);

    }


    if (pn == 4) {// Denne side referer til kategoriside der passer til sort mand 
        background(153, 204, 255);
        komFra = 1

        fill(255);
        stroke(1);
        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 125; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(ung1), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 275; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(ung2), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 425; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(ung3), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }
        noLoop();


        fill(0)
        rect(925, 15, 50, 25);
        image(img12, 0, 0, 1000, 600) // Dette billeder er bagggrunden på hver kategoriside 

        textAlign(LEFT);
        textFont('Futura');

        textSize(15);
        fill(0);
        text(myJson.shoppingpreferences, 20, 107, kategorix, kategoriy);
        text(myJson.culinaryrecommendations, 20, 257, kategorix, kategoriy);
        text(myJson.entertainment, 20, 407, kategorix, kategoriy);

    }

    if (pn == 5) {// Denne side referer til kategoriside der passer til den hvide mand 
        background(153, 204, 255);
        komFra = 2

        fill(255);
        stroke(1);
        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 125; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(mand1), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 275; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(mand2), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 425; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(mand3), x, y, 200, 100); //- rect er den rektangle der er blevet lavet 
        }
        noLoop();


        fill(0)
        rect(925, 15, 50, 25);
        image(img12, 0, 0, 1000, 600) // Dette billeder er bagggrunden på hver kategoriside 

        textAlign(LEFT);
        textFont('Futura');

        textSize(15);
        fill(0);
        text(myJson.shoppingpreferences, 20, 107, kategorix, kategoriy);
        text(myJson.culinaryrecommendations, 20, 257, kategorix, kategoriy);
        text(myJson.entertainment, 20, 407, kategorix, kategoriy);

    }

    if (pn == 6) {// Denne side referer til kategoriside der passer til LGBTQ+ pigen
        background(153, 204, 255);
        komFra = 3

        fill(255);
        stroke(1);
        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 125; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(kvinde1), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 275; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(kvinde2), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 425; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(kvinde3), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }
        noLoop();

        fill(0)
        rect(925, 15, 50, 25);
        image(img12, 0, 0, 1000, 600)// Dette billeder er bagggrunden på hver kategoriside 

        textAlign(LEFT);
        textFont('Futura');

        textSize(15);
        fill(0);
        text(myJson.shoppingpreferences, 20, 107, kategorix, kategoriy);
        text(myJson.culinaryrecommendations, 20, 257, kategorix, kategoriy);
        text(myJson.entertainment, 20, 407, kategorix, kategoriy);

    }

    if (pn == 7) {// Denne side referer til kategoriside der passer til gravid kvinde 
        background(153, 204, 255);
        komFra = 4

        fill(255);
        stroke(1);
        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 125; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(gravid1), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 275; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(gravid2), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 425; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(gravid3), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }
        noLoop();

        fill(0)
        rect(925, 15, 50, 25);
        image(img12, 0, 0, 1000, 600) // Dette billeder er bagggrunden på hver kategoriside 

        textAlign(LEFT);
        textFont('Futura');

        textSize(15);
        fill(0);
        text(myJson.shoppingpreferences, 20, 107, kategorix, kategoriy);
        text(myJson.culinaryrecommendations, 20, 257, kategorix, kategoriy);
        text(myJson.entertainment, 20, 407, kategorix, kategoriy);

    }

    if (pn == 8) {
        background(153, 204, 255);


        fill(255);
        stroke(1);
        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 125; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(noAlgo1), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 275; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(noAlgo2), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }

        for (let i = 0; i < 4; i++) { // for-loopet siger at når der skal laves rectangler i 
            let x = 15 + i * 250; // 170 er afstanden mellem firkanterne - 15 er startpositionen på den første firkant   
            let y = 425; // y placeringen er hvor langt nede på y aksen firkanten skal starte
            image(random(noAlgo3), x, y, 200, 100); // - rect er den rektangle der er blevet lavet 
        }
        noLoop();


        fill(0)
        rect(925, 15, 50, 25);
        image(img12, 0, 0, 1000, 600)

        textAlign(LEFT);
        textFont('Futura');

        textSize(15);
        fill(0);
        text(myJson.shoppingpreferences, 20, 107, kategorix, kategoriy);
        text(myJson.culinaryrecommendations, 20, 257, kategorix, kategoriy);
        text(myJson.entertainment, 20, 407, kategorix, kategoriy);


        image(img13, 0, 0, 1000, 600)

    }


}

console.log(picker)
console.log(enterPress)

function keyTyped() {
    if (key === 'Enter' && side == 1) {
        pn = 4
    }
    if (key === 'Enter' && side == 2) {
        pn = 5
    }
    if (key === 'Enter' && side == 3) {
        pn = 6
    }
    if (key === 'Enter' && side == 4) {
        pn = 7
    }


}

function keyPressed() {
    if (keyCode === 39) {
        picker = picker + 1
    } else if (keyCode === 37) {
        picker = picker - 1
    }
    if (keyCode === 13) {
        enterPress = true
    }
}
function mousePressed() {
    if (pn == 1) {
        pn = 12;
    }
    else if (pn == 12) {
        pn = 13;
    }
    else if (pn == 13) {
        pn = 14;
    }
    else if (pn == 14) {
        pn = 2;
    }
    else if (pn == 2) {
        pn = 3;
    }
    // refresh knap
    if (mouseX > 115 && mouseX < 138 && mouseY > 13 && mouseY < 42) {
        loop();
        frameRate(60);
    }
    //preview knap
    else if (pn == 8 && komFra == 1 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42) {
        pn = 4
        loop();
        noLoop();
    }
    else if (pn == 8 && komFra == 2 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42) {
        pn = 5
        loop();
        noLoop();
    }
    else if (pn == 8 && komFra == 3 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42) {
        pn = 6
        loop();
        noLoop();
    }
    else if (pn == 8 && komFra == 4 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42) {
        pn = 7
        loop();
        noLoop();
    } else if (pn == 4 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42 || pn == 5 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42|| pn == 6 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42|| pn == 7 && mouseX > 71 && mouseX < 102 && mouseY > 13 && mouseY < 42) {
        pn = 8
        loop();
        noLoop();
    }
    //tilbage knap

    if (mouseX > 33 && mouseX < 61 && mouseY > 13 && mouseY < 42) {
        pn = 3
        side = 0
        enterPress = false;
        loop();
    }
}